# Java-Mysql-Simple-Login-Web-application
This is a simple demonstration project to showcase conatinerization of Java web application and Mysql database in Docker and Kubernetes environment.

# Multi-stage Dockerfile which can be tested locally
docker build -t javawebapp:1.0.1 -f Dockerfile

# Deploy the PV and PVC of the YAML file
kubectl apply -f mysql-pv-pvc.yaml

# Deploy the contents of the YAML file
kubectl apply -f mysql_deployment.yaml

# Display information about the Deployment
kubectl describe deployment mysql

# List the pods created by the Deployment
kubectl get pods -l app=mysql

# Inspect the PersistentVolumeClaim
kubectl describe pvc mysql-pv-claim

# Accessing the MySQL instance and create table
kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql -ppassword

# fetch default pwd from mysql 
Run container in daemon mode & fetch the password from logs 
    docker run -d --name=mysql mysql/mysql-server:5.6
    docker logs mysql1 2>&1 | grep GENERATED
Output of above command is:
--> GENERATED ROOT PASSWORD: ********
```docker logs mysql 2>&1 | grep GENERATED
[Entrypoint] GENERATED ROOT PASSWORD: cYM0fw3KuLVun1@xc4LDUMteHOz
```
Once you have the password by one of the above-mentioned methods, you can then login with the below command using that password
kubectl exec -it mysql-66d4ccc94b-9rjtm -- /bin/bash
docker exec -it mysql mysql -u root -p

# mysql create database and table
CREATE DATABASE sample;
use sample;
CREATE TABLE USER (first_name varchar(255), last_name varchar(255), email varchar(255), username varchar(255), password varchar(255), regdate varchar(255));

# Prometheus installations using HELM Charts
Install helm2 in your local.

Add repositories
    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo add stable https://charts.helm.sh/stable

Update Helm repositories
    helm repo update

Install Prometheus Kubernetes
    helm install prometheus prometheus-community/kube-prometheus-stack

Kubernetes Prometheus Port Forward
    kubectl port-forward deployment/prometheus-grafana 3000

Log in to Grafana
    open localhost:3000
    username: admin
    password: prom-operator

