FROM maven:3.6.1-jdk-8 AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean install

FROM tomcat
COPY --from=build /usr/src/app/target/LoginWebApp.war /usr/local/tomcat/webapps/LoginWebApp.war
